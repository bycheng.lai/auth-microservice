package com.micro.auth.models;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collation = "roles") //to let mongoDB
@EqualsAndHashCode(callSuper = true)  //from lombok
public class MicroRoleModel extends GenericModel{

    private String role;

    public MicroRoleModel(){
        super(); // 呼叫父類構造方法
    }
}
